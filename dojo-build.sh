#!/bin/bash


# mustache project locations
ROOT=`pwd`
MUSTACHE_JS="${ROOT}/mustache.js/mustache.js"

# wrapper files
WRAPPER_DIR="${ROOT}/wrappers/dojo"
MUSTACHE_JS_PRE="${WRAPPER_DIR}/mustache.js.pre"
MUSTACHE_JS_POST="${WRAPPER_DIR}/mustache.js.post"
MUSTACHE_MAIN="${WRAPPER_DIR}/mustache.main.js"
MUSTACHE_PACKAGE="${WRAPPER_DIR}/mustache.package.json"
MUSTACHE_PROFILE="${WRAPPER_DIR}/mustache.profile.js"

BUILD_DIR="${ROOT}/build"
MUSTACHE_BUILD_DIR="${BUILD_DIR}/mustache"
MUSTACHE_JS_OUT="${MUSTACHE_BUILD_DIR}/engine.js"
MUSTACHE_PACKAGE_OUT="${MUSTACHE_BUILD_DIR}/package.json"
MUSTACHE_PROFILE_OUT="${MUSTACHE_BUILD_DIR}/mustache.profile.js"


function print () {
    printf "[DOJO-MUSTACHE BUILD] %s\n" "$1"
}

function rm_dir () {
    if [ -d "$1" ]; then
        rm -rf "$1"
    fi
}


function clean_mustache_build () {
    print "cleanup dojo-mustache build"
    rm_dir "${BUILD_DIR}"
}


function build_mustache () {
    # I've completely skipped mustache's own
    # custom build for dojo as it:
    #   a/ puts it under dojox
    #   b/ I've added a customized extension to mustache
    #      that's in the post-wrapper
    #   c/ their profile.json doesn't work out of the box
    #      with my additions
    if [ -e "${MUSTACHE_JS}" ]; then
        print "build mustache.js extension"
        cd "${ROOT}"
        mkdir -p "${MUSTACHE_BUILD_DIR}"
        cat $MUSTACHE_JS_PRE > $MUSTACHE_JS_OUT
        cat $MUSTACHE_JS >> $MUSTACHE_JS_OUT
        cat $MUSTACHE_JS_POST >> $MUSTACHE_JS_OUT
        # copy the compiled mustache.js and profile.json to mustache app dir
        cp $MUSTACHE_PACKAGE $MUSTACHE_PACKAGE_OUT
        cp $MUSTACHE_PROFILE $MUSTACHE_PROFILE_OUT
        cd ${BUILD_DIR}
        tar cvzf mustache.tar.gz mustache 
    else 
        print "${MUSTACHE_JS} doesn't exist! Did not proceed with build."
    fi
}


function main () {
    clean_mustache_build
    build_mustache
    cd "${ROOT}"
    print "dojo-mustache build complete!"
}


# call main
main
