var profile = (function(){
	var testResourceRe = /^mustache\/tests\//,

		copyOnly = function(filename, mid){
			var list = {
                'mustache/mustache.profile.js':true,
                'mustache/package.json':true
			};
			return (mid in list); 
		};

	return {
		resourceTags:{
			test: function(filename, mid){
				return testResourceRe.test(mid) || mid=="app/tests";
			},

			copyOnly: function(filename, mid){
				return copyOnly(filename, mid);
			},

			amd: function(filename, mid){
				return !testResourceRe.test(mid) && !copyOnly(filename, mid) && /\.js$/.test(filename);
			}
		}
	};
})();
